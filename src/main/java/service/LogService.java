/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import business.ExLog;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import layout.ExLayout;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
/**
 *
 * @author adria
 */
public class LogService {
    public JSONArray filmsToXml(){
        JSONArray exceptions = new JSONArray();
        try{
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbf.newDocumentBuilder();
            File f = new File("Errlog.xml");
            Document xml = builder.parse(f);
            xml.normalize();
            NodeList allEx = xml.getElementsByTagName("exception");
            for(int i = 0; i < allEx.getLength(); i++){
                Node node = allEx.item(i);
                Element ex = (Element)node;
                String message = ex.getElementsByTagName("message").item(0).getTextContent();
                String _class = ex.getElementsByTagName("class").item(0).getTextContent();
                String stackTrace = ex.getElementsByTagName("stackTrace").item(0).getTextContent();
                String datetime = ex.getElementsByTagName("datetime").item(0).getTextContent();
                List<String> vars = new ArrayList<>();
                NodeList variables = ex.getElementsByTagName("variable");
                for(int j = 0; j < variables.getLength(); j++){
                    Element var = (Element)variables.item(j);
                    String key = var.getAttribute("name");
                    String value = var.getTextContent();
                    String listItem = key + ": " + value;
                    vars.add(listItem);
                }
                
                exceptions.put(new ExLayout(datetime,stackTrace,message, vars).getJson()); 
            }
        }
        catch(Exception ex){
            new ExLog().handleException(ex);
        }
        return exceptions;
    }
}
