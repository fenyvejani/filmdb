/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import business.ExLog;
import business.Film;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import layout.ExLayout;
import layout.FilmLayout;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
/**
 *
 * @author adria
 */
public class FilmService {
        public JSONArray filmsToXml(){
        JSONArray films = new JSONArray();
        try{
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbf.newDocumentBuilder();
            File f = new File("films.xml");
            Document xml = builder.parse(f);
            xml.normalize();
            NodeList allEx = xml.getElementsByTagName("Film");
            for(int i = 0; i < allEx.getLength(); i++){
                Node node = allEx.item(i);
                Element ex = (Element)node;
                String title = ex.getElementsByTagName("title").item(0).getTextContent();
                String genre = ex.getElementsByTagName("genre").item(0).getTextContent();
                String director = ex.getElementsByTagName("director").item(0).getTextContent();
                String age_restrict = ex.getElementsByTagName("age_restrict").item(0).getTextContent();
                String release = ex.getElementsByTagName("release").item(0).getTextContent();
                String min = ex.getElementsByTagName("min").item(0).getTextContent();
                String available = ex.getElementsByTagName("available").item(0).getTextContent();
                
                
                films.put(new FilmLayout(title,genre,director, Integer.parseInt(age_restrict), release,Integer.parseInt(min), Integer.parseInt(available)).getJson()); 
            }
        }
        catch(Exception ex){
            new ExLog().handleException(ex);
        }
        return films;
    }
}
