/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fio;

/**
 *
 * @author adria
 */

import business.ExLog;
import business.GetterFunctionName;
import java.io.File;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import service.LogService;

public class Fio<T> {
    
    public void save(T entity){
        Class clazz = entity.getClass();
        try
        {
            File f = new File("films.xml");
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document xml = db.parse(f);
            Element film = xml.createElement("Film"); 
            Field[] tuls = clazz.getDeclaredFields();
            for(Field tul : tuls){
                if(tul.getAnnotation(GetterFunctionName.class)!=null){
                    String gfn = tul.getAnnotation(GetterFunctionName.class).name();
                    Method gm = clazz.getMethod(gfn);
                    String ertek = gm.invoke(entity).toString();
                    String valtozoNev = tul.getName();
                    Element elem = xml.createElement(valtozoNev);
                    elem.setTextContent(ertek);
                    film.appendChild(elem);
                }
            }

            film.setAttribute("class", clazz.getSimpleName());
            // XML-hez fűzés
            xml.getFirstChild().appendChild(film);
            
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer t = tf.newTransformer();
            DOMSource s = new DOMSource(xml);
            StreamResult r = new StreamResult(f);
            t.transform(s, r); 
            
        }
        catch(Exception ex){
            //System.out.println("An Error: " + ex.toString());
            ExLog el = new ExLog();
            el.variables = ExLog.setVariables();
            el.variables.put("Entity", entity.getClass());
            el.variables.put("count of Entity fields", entity.getClass().getFields().length);
            el.variables.put("Name of Entity", entity.getClass().getName());
            el.handleException(ex);
            System.err.println(ex.toString());
        }
        
    }
    
}
