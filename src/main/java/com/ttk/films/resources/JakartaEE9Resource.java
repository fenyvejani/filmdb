package com.ttk.films.resources;

import business.ExLog;
import business.Film;
import business.ReadTxt;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.print.attribute.standard.Fidelity;
import org.json.JSONArray;
import org.json.JSONObject;
import service.FilmService;
import service.LogService;

/**
 *
 * @author 
 */
@Path("jakartaee9")
public class JakartaEE9Resource {
    
    @GET
    @Path("filmsToXml")
    //@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response filmsToXml() throws Exception{
 
        if(Film.films.size() != 0){
            for (int i = 0; i < Film.films.size(); i++) {
                Film.films.get(i).Save();
                //System.out.println(Film.films.get(i).getDirector());
            }
            System.out.println("Films from list are saved to XML!");
            return Response.ok(new FilmService().filmsToXml().toString()).build();
        }
        
        //ArrayIndexOutOfBoundsException ai = new ArrayIndexOutOfBoundsException();
        Exception ex = new Exception();
        ExLog el = new ExLog();
        el.variables = ExLog.setVariables();
        el.variables.put("Class", Film.films.getClass().getName());
        el.variables.put("Count of Fields", Film.films.getClass().getFields().length);
        el.handleException(ex);
        System.err.println(ex.toString());
        return Response.ok(new LogService().filmsToXml().toString()).build();


    }
    
    
    @GET
    @Path("getFilms")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFilms(){
        
        if(Film.films.size() == 0){
            new ReadTxt("films.txt").filmsToList(Film.films);
            System.err.println("File reading is successful!");
            return Response.ok().status(Response.Status.OK).build();
        }
        System.err.println("Already used!");
        return Response.status(Response.Status.CONFLICT).build();
    }
}
