/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package business;

import fio.Fio;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author adria
 */
public class Film implements IFilm{
    
    public static List<Film>films = new ArrayList<>();
    
    @GetterFunctionName(name = "getTitle")
    private String title;
    @GetterFunctionName(name = "getGenre")
    private GenreEnum genre;
    @GetterFunctionName(name = "getDirector")
    private String director;
    @GetterFunctionName(name = "getAge_restrict")
    private Integer age_restrict;
    @GetterFunctionName(name = "getRelease")
    private String release; // Date
    @GetterFunctionName(name = "getMin")
    private Integer min; // In minutes
    @GetterFunctionName(name = "isAvailable")
    private Integer available;

    // New film
    
    public Film(String title, String genre, String director, String release, Integer min){
        this.title = title;
        this.genre = stringToEnum(genre);
        this.director = director;
        this.age_restrict = this.genre.getgenreAge();
        this.release = release;
        this.min = min;
        this.available = 1;
    }
    
    // Film import constructor
    public Film(String row){
        String[] splitted = row.split(";");
        this.title = splitted[0];
        this.genre = stringToEnum(splitted[1]);
        this.director = splitted[2];
        this.release = splitted[3];
        this.min = Integer.parseInt(splitted[4]);
        this.available = Integer.parseInt(splitted[5]);
    }
    
    public String getTitle() {
        return this.title;
    }

    // Convert GenreEnum to String
    public String getGenre() {
        String strGenre = "";
        switch(this.genre){
            case ACTION: strGenre = "Action"; break;
            case FANTASY: strGenre = "Fantasy"; break;
            case COMEDY: strGenre = "Comedy"; break;
            case ADVENTURE: strGenre = "Adventure"; break;
            case HORROR: strGenre = "Horror"; break;
            case THRILLER: strGenre = "Thriller"; break;
        }
        return strGenre;
    }
    
    // Convert a string to Enum
    private GenreEnum stringToEnum(String str){
        switch(str){
            case "Action": return GenreEnum.ACTION;
            case "Fantasy": return GenreEnum.FANTASY;
            case "Comedy": return GenreEnum.COMEDY;
            case "Adventure": return GenreEnum.ADVENTURE;
            case "Horror": return GenreEnum.HORROR;
            case "Thriller": return GenreEnum.THRILLER;
            default: return GenreEnum.ACTION;
        }
    }
    
    public String getDirector() {
        return this.director;
    }

    public Integer getAge_restrict() {
        return this.genre.getgenreAge();
    }

    public String getRelease() {
        return this.release;
    }

    public Integer getMin() {
        return this.min;
    }

    public Integer isAvailable() {
        return this.available;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    // Concat this object attributes then returning back.
    private String FilmObj(){
        String tmp = this.title + ";" + getGenre() + ";" + this.director + ";" + this.release + ";" + this.min.toString() + ";" + this.available.toString();
        return tmp;
    }
    
    
    // Override interface's method(s)
    @Override
    public Boolean Save() {
        if(Film.films.size() > 0){
            Fio<Film> f = new Fio<Film>();
            f.save(this);
            return true;
        }
        return false;
    }
    
}
