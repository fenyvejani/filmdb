/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package business;

/**
 *
 * @author adria
 */
public enum GenreEnum {
    ACTION(12), FANTASY(12), COMEDY(12), ADVENTURE(6), HORROR(18), THRILLER(18);
    
    private final Integer genreAge;
    
    private GenreEnum(Integer genreAge) {
        this.genreAge = genreAge;
    }
    
    public Integer getgenreAge(){
        return this.genreAge;
    }
}
