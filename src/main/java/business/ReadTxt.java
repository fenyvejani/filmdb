/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package business;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author adria
 */
public class ReadTxt {
    private String txtName;
    private Scanner sc;
    private String header;
    
    public ReadTxt(String txtName){
        this.txtName = txtName;
    }

    public String getTxtName() {
        return txtName;
    }

    public String getHeader() {
        return header;
    }
    
    public void filmsToList(List<Film>films){
        sc = new Scanner(new BufferedInputStream(getClass().getResourceAsStream("/"+this.txtName)));
        header = sc.nextLine();
        while(sc.hasNext()){
            Film.films.add(new Film(sc.nextLine()));
        }
        sc.close();
    }
}
