/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package layout;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author adria
 */
public class FilmLayout {
    
    public String title;
    public String genre;
    public String director;
    public Integer age_restrict;
    public String release; // Date
    public Integer min; // In minutes
    public String available;

    public FilmLayout(String title, String genre, String director, Integer age_restrict, String release, Integer min, Integer available) {
        this.title = title;
        this.genre = genre;
        this.director = director;
        this.age_restrict = age_restrict;
        this.release = release;
        this.min = min;
        if(available == 1) this.available = "yes";
        else this.available = "no";
    }
    
    public JSONObject getJson(){
        JSONObject filmobj = new JSONObject();
        filmobj.put("title",this.title);
        filmobj.put("genre",this.genre);
        filmobj.put("director",this.director);
        filmobj.put("age_restric",this.age_restrict);
        filmobj.put("release",this.release);
        filmobj.put("length",this.min);
        filmobj.put("available", this.available);

        return filmobj;

    }
}
