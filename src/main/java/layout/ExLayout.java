/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package layout;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
/**
 *
 * @author adria
 */
public class ExLayout {
    
    public String exDate;
    public String stackTrace;
    public String message;
    public List<String> variables;

    public ExLayout(String exDate, String stackTrace, String message, List<String> variables) {
        String[] dateData = exDate.split(" ");
        String newDate = dateData[5] + ". " + dateData[1] + ". " + dateData[2] + " " + dateData[3];
        this.exDate = newDate;
        this.stackTrace = stackTrace;
        this.message = message;
        this.variables = variables;
    }

    public String getExDate() {
        return exDate;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public String getMessage() {
        return message;
    }
    
    public JSONObject getJson(){
        JSONObject exception = new JSONObject();
        exception.put("message",this.message);
        exception.put("stackTrace",this.stackTrace);
        exception.put("datetime",this.exDate);
        JSONArray variables = new JSONArray();
        for(String var: this.variables){
            JSONObject vardata = new JSONObject();
            vardata.put("var", var);
            variables.put(var);
        }
        exception.put("variables", variables);
        return exception;

    }
}
